#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

int SCREEN_HEIGHT = 500;
int SCREEN_WIDTH = 500;

void drawText(SDL_Renderer *r, char *text) {
    // open font
    TTF_Font* renderFont = TTF_OpenFont("./font/Orbitron Light.ttf", 72);
    if (renderFont == NULL) {
        printf("Error loading font.\n");
        printf("%s\n",TTF_GetError());
    }
    // set color
    SDL_Color renderColor = {255, 255, 255};
    // render text to surface
    SDL_Surface* renderedTextSurface = TTF_RenderText_Solid(renderFont, text, renderColor);
    if (renderedTextSurface == NULL) {
        printf("Error generating text surface.\n");
        printf("%s\n",TTF_GetError());
    }
    // convert surface to texture
    SDL_Texture* renderedTextTexture = SDL_CreateTextureFromSurface(r, renderedTextSurface);
    if (renderedTextTexture == NULL) {
        printf("Error creating texture from surface.\n");
        printf("%s\n",TTF_GetError());
    }
    // set text location
    SDL_Rect textLocation;
    textLocation.x = 0;
    textLocation.y = 0;
    textLocation.w = SCREEN_WIDTH;
    textLocation.h = SCREEN_HEIGHT;
    // clear renderer
    SDL_SetRenderDrawColor(r, 0, 0, 0, 255);
    SDL_RenderClear(r);
    // copy text to renderer
    SDL_RenderCopy(r, renderedTextTexture, NULL, &textLocation);
    // refresh renderer
    SDL_RenderPresent(r);
    // delete trash
    SDL_DestroyTexture(renderedTextTexture);
    SDL_FreeSurface(renderedTextSurface);
}

void stop(SDL_Window *w, SDL_Renderer *r) {
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    exit(0);
}

int main(int argc, char ** argv) {
    // declare variables for main
    SDL_Event event;

    // init SDL
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window * screen = SDL_CreateWindow("Text Demo",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH, SCREEN_HEIGHT,
        SDL_WINDOW_RESIZABLE
    );
    SDL_Renderer * renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_ACCELERATED);
    SDL_Surface * surface = SDL_GetWindowSurface(screen);
    SDL_SetSurfaceRLE(surface, 1);
    TTF_Init();
    
    // enter main loop
    while (true) {
        // get events
        SDL_PollEvent(&event);
        // process event queue
        switch (event.type) {
            case SDL_QUIT:
                stop(screen, renderer);
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                    SCREEN_WIDTH = event.window.data1;
                    SCREEN_HEIGHT = event.window.data2;
                    continue;
                }
                break;
            default:
                break;
        }
        // render screen
        char text[] = "This is a test!";
        drawText(renderer, text);
    }
}