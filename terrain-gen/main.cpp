// import libraries
#include <SDL2/SDL.h>
#include <iostream>
#include <chrono>
#include <vector>
#include <random>

// settings
int screen_height = 500;               // height of the program window (y axis)
int screen_width = 1000;               // width of the program window (x axis)
double decay_rate = 0.2;               // percent of land to remove
double build_rate = 0.05;              // percent of sediment to place
double rain_amount = 4;                // amount of water in each rain droplet
double evaporation_rate = 0.7;         // percentage of water that will evaporate
double flow_amount = 0.3;              // percentage of water that will move
double max_elevation = 100;            // highest elevation for terrain to naturally generate
double water_droplets_per_frame = 0.2; // percent of screen to place water droplets on
double water_updates_per_frame = 0.25; // percent of screen to refresh per frame

// container for world information
struct WorldData
{
    double elevation;       // elevation of the land
    double water_amount;    // amount of water on the land
    double sediment_amount; // amount of land to be placed
};

// container for a point in the world
struct Point2D
{
    int x; // the x coordinate of a point
    int y; // the y coordinate of a point
};

// grid representing the entire world
std::vector<std::vector<WorldData>> world;

// declare required function in advance
std::chrono::nanoseconds getTime();

// initialize clock delta
std::chrono::nanoseconds CLOCK_OLD = getTime();
std::chrono::nanoseconds CLOCK_NEW = getTime();
float CLOCK_DELTA = 0;

/**
 * @brief  This shuts down the SDL library
 * @note   This program can not recover after this is called
 * @param  r: the sdl renderer
 * @param  w: the sdl window
 * @retval None
 */
void stop(SDL_Renderer *r, SDL_Window *w)
{
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    SDL_Quit();
    exit(0);
}

/**
 * @brief  This gets the current time in nanoseconds
 * @retval the current time in nanoseconds
 */
std::chrono::nanoseconds getTime()
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch());
}

/**
 * @brief  Generates a world using random noise
 * @note   The old world information will be deleted
 */
void generate_world()
{
    // delete the current world
    if (world.size())
    {
        for (auto c = world.begin(); c != world.end(); c++)
        {
            c->clear();
        }
        world.clear();
    }
    // iterate over each land position
    for (int y = 0; y < screen_height; y++)
    {
        // add another row
        world.push_back(std::vector<WorldData>());
        for (int x = 0; x < screen_width; x++)
        {
            // initialize a WorldData object
            WorldData wd;
            // set the elevation to a random height
            wd.elevation = (rand() / (double)RAND_MAX) * max_elevation;
            // do not put water on the world
            wd.water_amount = 0;
            // do not put sediment on the word
            wd.sediment_amount = 0;
            // add the worldData information to the array
            world[y].push_back(wd);
        }
    }
}

/**
 * @brief  Get a random point on the screen
 * @retval a random point on the screen
 */
Point2D rand_point()
{
    Point2D p;
    p.y = rand() % screen_height;
    p.x = rand() % screen_width;
    return p;
}

/**
 * @brief  Change a value without becoming negative
 * @note   Make delta negative to subtract
 * @param  original: the amount to be modified
 * @param  delta: the amount to change by
 * @retval a modified value no less than 0
 */
double modify_amount(double original, double delta)
{
    double new_amount = original + delta;
    if (new_amount < 0)
    {
        new_amount = 0;
    }
    return new_amount;
}

/**
 * @brief  Pick an adjacent point with the lowest elevation.
 * @note   It will return the original point if it is the lowest.
 * @param  original: the point being examined
 * @retval the lowest neighboring point
 */
Point2D lowest_neighbor(Point2D original)
{
    double smallest_elevation = world[original.y][original.x].elevation;
    Point2D smallest_point = original;
    for (int y = original.y - 1; y < original.y + 2; y++)
    {
        for (int x = original.x - 1; x < original.x + 2; x++)
        {
            if (y < 0 || y >= screen_height)
            {
                continue;
            }
            if (x < 0 || x >= screen_width)
            {
                continue;
            }
            if (world[y][x].elevation < smallest_elevation)
            {
                smallest_elevation = world[y][x].elevation;
                smallest_point.x = x;
                smallest_point.y = y;
            }
        }
    }
    return smallest_point;
}

/**
 * @brief  Simulates the erosion of land over time
 * @note   This can take a long time depending on settings
 */
void update_world()
{
    // process water droplet placements
    for (int i = 0; i < screen_height * screen_width * water_droplets_per_frame; i++)
    {
        // place a water droplet at a random spot
        Point2D target = rand_point();
        world[target.y][target.x].water_amount += rain_amount;
    }
    // process water update events
    for (int i = 0; i < screen_height * screen_width * water_updates_per_frame; i++)
    {
        // get old coordinate
        Point2D old_point = rand_point();
        // does water exist?
        if (world[old_point.y][old_point.x].water_amount == 0)
        {
            // place all sediment
            world[old_point.y][old_point.x].elevation += world[old_point.y][old_point.x].sediment_amount;
            world[old_point.y][old_point.x].sediment_amount = 0;
        }
        else
        {
            // remove land
            double land_removing = decay_rate * world[old_point.y][old_point.x].water_amount * CLOCK_DELTA;
            world[old_point.y][old_point.x].elevation = modify_amount(world[old_point.y][old_point.x].elevation, land_removing * -1);
            world[old_point.y][old_point.x].sediment_amount = modify_amount(world[old_point.y][old_point.x].sediment_amount, land_removing);
            // pick up sediment (NOTE: moving sediment uses the same formula as water since sediment is in the water)
            double sediment_moving = flow_amount * world[old_point.y][old_point.x].water_amount * CLOCK_DELTA;
            world[old_point.y][old_point.x].sediment_amount = modify_amount(world[old_point.y][old_point.x].sediment_amount, sediment_moving * -1);
            // pick up water
            double water_moving = flow_amount * world[old_point.y][old_point.x].water_amount * CLOCK_DELTA;
            world[old_point.y][old_point.x].water_amount = modify_amount(world[old_point.y][old_point.x].water_amount, water_moving * -1);
            // get new coordinate
            Point2D new_point = lowest_neighbor(old_point);
            // place water
            world[new_point.y][new_point.x].water_amount = modify_amount(world[new_point.y][new_point.x].water_amount, water_moving);
            // place sediment
            world[new_point.y][new_point.x].sediment_amount = modify_amount(world[new_point.y][new_point.x].sediment_amount, sediment_moving);
            // place land
            double land_placing = build_rate * world[new_point.y][new_point.x].sediment_amount * CLOCK_DELTA;
            world[new_point.y][new_point.x].sediment_amount = modify_amount(world[new_point.y][new_point.x].sediment_amount, land_placing * -1);
            world[new_point.y][new_point.x].elevation = modify_amount(world[new_point.y][new_point.x].elevation, land_placing);
            // evaporate
            world[new_point.y][new_point.x].water_amount = modify_amount(world[new_point.y][new_point.x].water_amount, evaporation_rate * world[new_point.y][new_point.x].water_amount * -1);
        }
    }
}

/**
 * @brief  Draw the world onto the screen
 * @note   World elevation should not exceed the max_elevation setting
 * @param  render: the sdl renderer
 */
void render_world(SDL_Renderer *render)
{
    // clear the screen
    SDL_SetRenderDrawColor(render, 255, 100, 255, 255);
    SDL_RenderClear(render);
    // render each pixel according to the height value
    for (int y = 0; y < screen_height; y++)
    {
        for (int x = 0; x < screen_width; x++)
        {
            // get the color value
            int color = (world[y][x].elevation / max_elevation) * 255;
            // do not change color if illegal
            if (color >= 0 && color <= 255)
            {
                SDL_SetRenderDrawColor(render, color, color, color, 255);
            }
            SDL_RenderDrawPoint(render, x, y);
        }
    }
    // present the new render
    SDL_RenderPresent(render);
}

/**
 * @brief  Calulcate the elapsed time since the last call
 * @note   The result is stored in CLOCK_DELTA
 */
void calcDeltaTime()
{
    CLOCK_OLD = CLOCK_NEW;
    CLOCK_NEW = getTime();
    CLOCK_DELTA = (CLOCK_NEW - CLOCK_OLD).count() * 0.000000001;
}

/**
 * @brief  The main driver for the program
 * @param  argc: number of args
 * @param  argv: array of args
 * @retval exit status
 */
int main(int argc, char **argv)
{
    // initialize the window
    SDL_Window *window = SDL_CreateWindow(
        "Terrain Generator", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        screen_width,
        screen_height,
        SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

    // initialize the renderer
    SDL_Renderer *renderer = NULL;
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    // initialize the random number generator
    srand(getTime().count());

    // generate the world
    generate_world();

    // begin main loop
    bool quit = false;
    SDL_Event event;
    while (!quit)
    {
        // get events
        SDL_PollEvent(&event);
        // process events
        switch (event.type)
        {
        case SDL_QUIT:
            // CASE: window was closed
            quit = true;
            break;
        case SDL_WINDOWEVENT:
            if (event.window.event == SDL_WINDOWEVENT_RESIZED)
            {
                // CASE: window was resized
                // change global vars
                screen_width = event.window.data1;
                screen_height = event.window.data2;
                // generate a new world
                generate_world();
                // skip additional processing
                continue;
            }
            break;
        }
        // read keyboard data
        const Uint8 *keyboard = SDL_GetKeyboardState(NULL);
        if (keyboard[SDL_SCANCODE_ESCAPE])
        {
            quit = true;
        }
        if (keyboard[SDL_SCANCODE_R])
        {
            generate_world();
        }
        // update the world
        update_world();
        // render the world
        render_world(renderer);
        // update time
        calcDeltaTime();
        // print the time difference
        printf("\rdelta: %.4f", CLOCK_DELTA);
    }

    stop(renderer, window);

    return EXIT_SUCCESS;
}
