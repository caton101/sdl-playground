// This is the SDL2 library. It is used for graphics.
#include <SDL2/SDL.h>
// This is the Chrono library. It is used to track time.
#include <chrono>

// make accessing chrono easier
using namespace std::chrono;

// make pointers to important information
SDL_Window *sdlWindow;     // this draws objects to the window
SDL_Renderer *sdlRenderer; // this represents the application window

// settings
int windowWidth = 1000;
int windowHeight = 500;
double rotationDelta = 0.1;
double rotationMod = 1;
int lineSize = 200;

// declare the time accessor in advance
nanoseconds getTime();
// make variables to store time information
nanoseconds CLOCK_OLD = getTime();
nanoseconds CLOCK_NEW = getTime();
double CLOCK_DELTA = 0;

/**
 * This returns the current time in nanoseconds.
 */
nanoseconds getTime()
{
    return duration_cast<nanoseconds>(system_clock::now().time_since_epoch());
}

/**
 * This calculates the time delta.
 * @note this uses the global variables CLOCK_OLD, CLOCK_NEW, and CLOCK_DELTA
 */
void calcDeltaTime()
{
    // replace the old time
    CLOCK_OLD = CLOCK_NEW;
    // store the current time
    CLOCK_NEW = getTime();
    // calculate the new delta
    // NOTE: 0.000000001 converts nanoseconds to seconds
    CLOCK_DELTA = (CLOCK_NEW - CLOCK_OLD).count() * 0.000000001;
}

/**
 * This function initializes SDL and creates all needed structs.
 */
void init()
{
    // initialize SDL2
    SDL_Init(SDL_WINDOW_OPENGL);
    // make the window
    sdlWindow = SDL_CreateWindow(
        "SDL2 Demo",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        windowWidth, windowHeight,
        SDL_WINDOW_RESIZABLE);
    // make the renderer for drawing to the window
    sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED);
}

void render()
{
    // make rotaton container
    static double rotation = 0;
    // clear screen
    SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
    SDL_RenderClear(sdlRenderer);
    // calc line positons
    SDL_SetRenderDrawColor(sdlRenderer, sin(rotation) * 255, cos(rotation) * 255, tan(rotation) * 255, 255);
    int y1 = cos(rotation) * lineSize;
    int y2 = y1 * -1;
    int x1 = sin(rotation) * lineSize;
    int x2 = x1 * -1;
    // translate to the center of the screen
    int midX = windowWidth / 2;
    int midY = windowHeight / 2;
    y1 += midY;
    y2 += midY;
    x1 += midX;
    x2 += midX;
    SDL_RenderDrawLine(sdlRenderer, x1, y1, x2, y2);
    // calc new rotation
    rotation += rotationDelta * CLOCK_DELTA;
    // show the new frame
    SDL_RenderPresent(sdlRenderer);
}

void run()
{
    // make a container to store events
    SDL_Event event;
    // make a flag to kill the game
    bool quit = false;
    // enter the main render loop
    while (!quit)
    {
        // get the clock delta
        calcDeltaTime();
        // process all events in the queue
        while (SDL_PollEvent(&event))
        {
            // check the event type
            switch (event.type)
            {
            case SDL_QUIT:
                // this event means the operating system asked the program to stop
                quit = true;
                // end the switch case
                break;
            case SDL_WINDOWEVENT:
                // this event means the operating system modified the window
                if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                {
                    // the window size was modified
                    // NOTE: the new screen position is inside the event data
                    windowWidth = event.window.data1;
                    windowHeight = event.window.data2;
                }
                // end the switch case
                break;
            default:
                break;
            }
        }
        // read keyboard data
        const Uint8 *keyboard = SDL_GetKeyboardState(NULL);
        if (keyboard[SDL_SCANCODE_ESCAPE] || keyboard[SDL_SCANCODE_Q]) {
            return;
        }
        if (keyboard[SDL_SCANCODE_EQUALS]) {
            rotationDelta += rotationMod * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_MINUS]) {
            rotationDelta -= rotationMod * CLOCK_DELTA;
        }
        // render the screen
        render();
        // print info to the terminal
        printf("\rclockDelta: %.9f    rotationDelta: %.9f", CLOCK_DELTA, rotationDelta);
        //SDL_Delay(50);
    }
}

int main()
{
    // initialize graphics
    init();
    // run the scene
    run();
    // close the program
    return 0;
}
