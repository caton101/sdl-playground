#include <SDL2/SDL.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_video.h>
#include <iostream>
#include <string>

// use globals to speed it up
SDL_Renderer *renderer = nullptr;
SDL_Window *window = nullptr;
SDL_Surface *surface = nullptr;
SDL_Texture *tex = nullptr;
SDL_Rect window_size = {0,0,0,0};
char *texture = nullptr;

typedef struct color_t {
    int red;
    int green;
    int blue;
} color_t;

void stop() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_DestroyTexture(tex);
    delete surface;
    delete[] texture;
    exit(0);
}

void processSDLEvents() {
    SDL_Event event;
    SDL_PollEvent(&event);
 
    switch (event.type)
    {
        case SDL_QUIT:
            stop();
            break;
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym) {
                case SDLK_q:
                    stop();
                    break;
                case SDLK_ESCAPE:
                    stop();
                    break;
            }
            break;
    }
}

int getTextureAddress(int x, int y) {
    return ((window_size.w*y)+x)*3;
}

void editTexture(color_t &color, int x, int y) {
    int texture_address = getTextureAddress(x, y);
    texture[texture_address]   = color.red;
    texture[texture_address+1] = color.green;
    texture[texture_address+2] = color.blue;
}

void renderTexture() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    surface = SDL_CreateRGBSurfaceFrom(
        texture,
        window_size.w,
        window_size.h,
        3*8,
        window_size.w*3,
        0x0000FF,
        0x00FF00,
        0xFF0000,
        0
    );
    tex = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_RenderCopy(renderer, tex, &window_size, &window_size);
    SDL_RenderPresent(renderer);
    SDL_DestroyTexture(tex);
    delete surface;
}

void drawPoint(color_t &color, int x, int y) {
    SDL_SetRenderDrawColor(renderer, color.red, color.green, color.blue, 255);
    SDL_RenderDrawPoint(renderer, x, y);
    SDL_RenderPresent(renderer);
}

int main(int argc, char ** argv)
{
    // read header
    std::string format;
    std::cin >> format;
    if (format != "P3") {
        std::cout << "[ERROR] Incompatible format: \"" << format << "\"\n";
        return 1;
    }

    // read size
    window_size.x = 0;
    window_size.y = 0;
    std::cin >> window_size.w >> window_size.h;
    std::cout << "[INFO] Set window to W=" << window_size.w << ", H=" << window_size.h << "\n";

    // get max color
    int maxcolor;
    std::cin >> maxcolor;
    if (maxcolor != 255) {
        std::cout << "[ERROR] Incompatible max color value " << maxcolor << "\n";
        return 1;
    }

    // init SDL2    
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow(
        "PPM Renderer",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        window_size.w,
        window_size.h,
        SDL_WINDOW_VULKAN
    );
    renderer = SDL_CreateRenderer(
        window,
        -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_ACCELERATED
    );

    // make a texture to edit
    texture = new char[window_size.h * window_size.w * 3];
    for (int y = 0; y < window_size.h; y++) {
        for (int x = 0; x < window_size.w; x++) {
            int addr = getTextureAddress(x, y);
            texture[addr]   = 0;
            texture[addr+1] = 0;
            texture[addr+2] = 0;
        }
    }

    // read image
    color_t color;
    for (int y = 0; y < window_size.h; y++) {
        for (int x = 0; x < window_size.w; x++) {
            // read pixel information
            std::cin >> color.red >> color.green >> color.blue;
            // edit the texture
            editTexture(color, x, y);
            // render the dot for now
            drawPoint(color, x, y);
            // process SDL event
            processSDLEvents();
        }
    }

    // detect quit event
    while (true)
    {
        // render screen
        renderTexture();
        // process SDL event
        processSDLEvents();
    }
}
